import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login'
import Home from "../views/Home"
import RadiationData from "../views/RadiationData";
import AccountData from "../views/AccountData";
import Equipment from "../views/Equipment";
import Area from "../views/Area";
import Me from "../views/Me";
import SlotBox from "../views/SlotBox";

Vue.use(VueRouter)
const routes = [
    {
        path: '/',
        redirect: '/login',
        hidden: true
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        hidden: true
    },
    {
        path: '/home',
        name: 'Home',
        //component: () => import('../views/Home.vue') 如果要用懒加载,这样写..
        component: Home,
        meta: {
            //senior控制不同权限账户下的导航显示
            senior: false
        },
        children:[
            {
                path: '/manage',
                name: '数据管理',
                icon: 'el-icon-menu',
                meta: {
                    senior: false,
                    menuItem: false
                },
                component: SlotBox,
                //当父节点不需要设置component时,这样设置可以让其映射子节点的内容
                /*component: {
                    render: e => e("router-view")
                },*/
                children:[
                    {
                        path: '/manage/radiation',
                        name: '辐射值数据',
                        icon: 'el-icon-s-data',
                        component: RadiationData,
                        meta: {
                            keepAlive: true,
                            senior: false,
                            menuItem: true
                        }
                    },
                    {
                        path: '/manage/account',
                        name: '账户管理',
                        icon: 'el-icon-s-custom',
                        component: AccountData,
                        meta: {
                            keepAlive: true,
                            senior: true,
                            menuItem: true
                        }
                    },
                    {
                        path: '/manage/equipment',
                        name: '设备相关',
                        icon: 'el-icon-s-platform',
                        component: Equipment,
                        meta: {
                            keepAlive: true,
                            senior: false,
                            menuItem: true
                        }
                    },
                    {
                        path: '/manage/area',
                        name: '地区相关',
                        icon: 'el-icon-location-information',
                        component: Area,
                        meta: {
                            keepAlive: true,
                            senior: false,
                            menuItem: true
                        }
                    }
                ]
            },
            {
                path: '/setting',
                name: '设置',
                icon: 'el-icon-setting',
                meta: {
                    senior: false,
                    menuItem: false
                },
                component: SlotBox,
                /*component: {
                    render: e => e("router-view")
                },*/
                children: [
                    {
                        path: '/setting/me',
                        name: '个人中心',
                        icon: 'el-icon-s-home',
                        component: Me,
                        meta: {
                            keepAlive: true,
                            senior: false,
                            menuItem: true
                        }
                    }
                ]
            }
        ]
    }
]

const router = new VueRouter({
    base: '/RadiationDataPlatform',
    routes
})

export default router
