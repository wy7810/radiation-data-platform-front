import axios from "axios";
import {Message} from "element-ui";
import router from "../router/index";

//全局的请求拦截器,相当于封装了一下,方便以后的调用
axios.interceptors.request.use(config=>{
    //先判断本地的sessionStorage中是否存在token
    if(window.sessionStorage.getItem('tokenStr')){
        //将token放在请求头中
        config.headers['authorization'] = window.sessionStorage.getItem('tokenStr');
    }
    return config;
},error => {
    console.log(error);
})

//全局的响应拦截器
axios.interceptors.response.use(success=>{
    //业务逻辑判断
    if(success.status && success.status === 200) {
        if (success.data.code === 500 || success.data.code === 403 || success.data.code === 401) {
            Message.error({message: success.data.message});
            return;
        }
        if (success.data.message) {
            Message.success({message: success.data.message});
        }
    }
    //返回数据给请求的方法
    return success.data;
},error => {
    if (error.response.code === 504 || error.response.code === 404) {
        Message.error({message: '服务器访问失败!'});
    } else if (error.response.code === 403) {
        Message.error({message: '权限不足,请联系管理员'});
    } else if (error.response.code === 401) {
        Message.error({message: '尚未登录,请先登录'});
        //跳转去登陆页面
        router.replace('/');
    } else {
        if (error.message.data.message) {
            Message.error({message: error.message.data.message});
        } else {
            Message.error({message: '未知错误'});
        }
    }
});

//前置路径,例如大型项目中每个功能都有不同的服务器来处理时,可以通过更改这个{base}来批量更改url
//默认为空,则表示不需要前置路径
let base = '';

//发送json格式的post请求(全局设置,项目中所有的axios请求都从这里发,而在其他文件中只需要调用这个方法即可
export const postRequest=(url,params)=> {
    return axios({
        method: 'post',
        url: `${base}${url}`,
        data: params
    })
}

//传送json格式的get请求
export const getRequest = (url,params)=>{
    return axios({
        method: 'get',
        url: `${base}${url}`,
        data: params
    })
}

//传送json格式的put请求
export const putRequest = (url,params)=>{
    return axios({
        method: 'put',
        url: `${base}${url}`,
        data: params
    })
}

//传送json格式的delete请求
export const deleteRequest = (url,params)=>{
    return axios({
        method: 'delete',
        url: `${base}${url}`,
        data: params
    })
}