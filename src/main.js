import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store'
import ElementUI from 'element-ui'
import AMap from "vue-amap";
import 'element-ui/lib/theme-chalk/index.css'
import 'font-awesome/css/font-awesome.css'
import md5 from "js-md5";

import {getRequest} from "./utils/api";
import {postRequest} from "./utils/api";
import {putRequest} from "./utils/api";
import {deleteRequest} from "./utils/api";

Vue.config.productionTip = false
Vue.use(ElementUI);

//全局应用js-md5
Vue.prototype.$md5 = md5;

//vue-amap
Vue.use(AMap);
//初始化
AMap.initAMapApiLoader({
    //自己在高德地图上申请的项目Key
    key: "a21d8210963a900161be75c4aca1e75b",
    //功能插件(按需选择)
    plugin: [
        "AMap.Autocomplete", //输入提示插件
        "AMap.PlaceSearch", //POI搜索插件
        "AMap.Scale", //右下角缩略图插件 比例尺
        "AMap.OverView", //地图鹰眼插件
        "AMap.ToolBar", //地图工具条
        "AMap.MapType", //类别切换控件，实现默认图层与卫星图、实施交通图层之间切换的控制
        "AMap.PolyEditor", //编辑 折线多，边形
        "AMap.CircleEditor", //圆形编辑器插件
        "AMap.Geocoder", //逆编码插件(通过经纬度获取区域名)
        "AMap.Geolocation", //定位控件，用来获取和展示用户主机所在的经纬度位置
    ],
    v: '1.4.4'
});

//以插件的形式来使用封装好的请求,以后在其他页面进行请求调动时,直接this.xxxRequest就行了
Vue.prototype.getRequest = getRequest;
Vue.prototype.postRequest = postRequest;
Vue.prototype.putRequest = putRequest;
Vue.prototype.deleteRequest = deleteRequest;

//vue-router导航守卫
router.beforeEach((to,from,next)=>{
    //判断有没有tokenStr存在本地,如果有,则允许其他页面的访问跳转
    if(window.sessionStorage.getItem("tokenStr")) {
        //判断本地的sessionStorage中是否有账户信息,如果没有就去申请并存入本地
        if (!window.sessionStorage.getItem("loginInfo")) {
            // return getRequest('/api/user/info').then(resp => {
            return getRequest('/user/info').then(resp => {
                if (resp) {
                    //存入用户信息
                    window.sessionStorage.setItem("loginInfo", JSON.stringify(resp));
                    //检查想访问是否属于当前账户权限的内容,如果不属于就跳转至Home
                    if (to.meta.senior && store.state.static.loginInfo.levelId !== 1) {
                        next('/home');
                    }
                    next();
                }
            })
        }
        //检查想访问是否属于当前账户权限的内容,如果不属于就跳转至Home
        if (to.meta.senior && store.state.static.loginInfo.levelId !== 1) {
            next('/home')
        }
        next();
    }else {
        //如果没有tokenStr,又想访问除了登录页面以外的页面,就强制跳转去登陆
        if(to.path==='/'||to.path==='/login'){
            next();
        }else{
            next('/?redirect='+to.path)
        }
    }
})

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
