// 通过mutation间接更新state的多个方法的对象
import state from "./state";

import {
    getRequest,
    postRequest,
    putRequest,
    deleteRequest
} from "../utils/api";

import {
    //个人设置
    UPDATE_LOGIN_INFO,
    UPDATE_LOGIN_PROFILE,
    //账户管理
    UPDATE_ACCOUNTS_PARAMS,
    RECEIVE_ACCOUNTS,
    CONCAT_ACCOUNT,
    CONCAT_ACCOUNTS,
    UPDATE_ACCOUNT_INFO,
    //区域管理
    UPDATE_AREA_PARAMS,
    RECEIVE_AREAS,
    CONCAT_AREA,
    UPDATE_AREA_INFO,
    UPDATE_AREA_COUNT,
    DELETE_AREA_INFO,
    //设备管理
    UPDATE_EQUIPMENT_PARAMS,
    RECEIVE_EQUIPMENTS,
    CONCAT_EQUIPMENT,
    CONCAT_EQUIPMENTS,
    UPDATE_EQUIPMENT_INFO,
    //数据管理
    UPDATE_DATA_PARAMS,
    RECEIVE_DATA,
    CONCAT_DATA,
    UPDATE_DATA_INFO,
    RECEIVE_BASE_STATISTICS_DATA,
    RECEIVE_STATISTICS_DATA,
    //记录管理
    RECEIVE_RECORDS
} from "./mutationTypes";

export default {
    /*------------------------------------个人设置------------------------------------*/
    //更新本地的登录者信息
    updateLoginInfo({commit}, params) {
        commit(UPDATE_LOGIN_INFO, {params});
    },
    async getProfileUrl({commit}) {
        //发送异步ajax请求
        // await getRequest("/api/user/profile").then(resp => {
        await getRequest("/user/profile").then(resp => {
            commit(UPDATE_LOGIN_PROFILE, resp);
        })
    },
    //异步提交账户信息更改
    async submitInfoSetting({commit}, params) {
        return new Promise(function (resolve, reject) {
            // postRequest("/api/userManage/editUser", params).then(resp => {
            postRequest("/userManage/editUser", params).then(resp => {
                if (resp) {
                    //提交mutation更新本地登录账户信息
                    commit(UPDATE_LOGIN_INFO, {params});
                    //同时更新sessionStorage中关于登录者信息的数据
                    window.sessionStorage.setItem("loginInfo", JSON.stringify(params));
                    //返回成功信号
                    resolve(true);
                } else {
                    reject(false);
                }
            })
        })
    },
    //异步提交密码更改申请
    async submitPwdSetting({commit}, params) {
        return new Promise(function (resolve, reject) {
            //提交修改请求
            // putRequest("/api/userManage/pwdSet", params).then(resp => {
            putRequest("/userManage/pwdSet", params).then(resp => {
                if (resp) {
                    resolve(true);
                } else {
                    reject(false);
                }
            })
        })
    },

    /*------------------------------------账户管理------------------------------------*/
    //调整账户页面参数
    updateAccountsParam({commit}, params) {
        commit(UPDATE_ACCOUNTS_PARAMS, {params});
    },
    //异步获取账户信息(分页获取)
    async initAccounts({commit}) {
        //判断是否有Account数据,如果有则略过避免重复请求
        if(!state.active.accountData.data){
            //发送异步ajax请求
            // await getRequest("/api/userManage/getUserListWithPage?currentPage=1&size=20").then(resp => {
            await getRequest("/userManage/getUserListWithPage?currentPage=1&size=20").then(resp => {
                if (resp) {
                    //根据结果提交一个mutation
                    const accounts = resp;
                    commit(RECEIVE_ACCOUNTS, {accounts});
                }
            })
        }
    },
    //异步搜索账户信息
    async selectAccounts({commit}) {
        const params = state.static.accountListParams;
        //动态生成url
        // let url = "/api/userManage/getUserListWithPage?currentPage=1&size=20";
        let url = "/userManage/getUserListWithPage?currentPage=1&size=20";
        if (params.userId !== '') {
            url += "&userId=" + params.userId;
        }
        if (params.userName !== '') {
            url += "&userName=" + params.userName;
        }
        if (params.levelId !== '') {
            url += "&levelId=" + params.levelId;
        }
        if (params.loginIp !== '') {
            url += "&loginIp" + params.loginIp;
        }
        if (params.DateScope.length === 2) {
            url += "&DateScope=" + params.DateScope[0] + "&DateScope=" + params.DateScope[1];
        }
        //发送异步ajax请求
        await getRequest(url).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const accounts = resp;
                commit(RECEIVE_ACCOUNTS, {accounts});
            }
        })
    },
    //异步新增单条账户信息
    async addAccount({commit}, userParam) {
        //发送异步ajax请求
        // await postRequest('/api/userManage/addUser', userParam).then(resp => {
        await postRequest('/userManage/addUser', userParam).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const newAccount = resp.obj;
                commit(CONCAT_ACCOUNT, {newAccount});
            }
        })
    },
    //异步获取下一页数据并新增到账户信息
    async addAccounts({commit}) {
        const params = state.static.accountListParams;
        //动态生成带参数的请求url
        // let url = "/api/userManage/getUserListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        let url = "/userManage/getUserListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        if (params.userId !== '') {
            url += "&userId=" + params.userId;
        }
        if (params.userName !== '') {
            url += "&userName=" + params.userName;
        }
        if (params.levelId !== '') {
            url += "&levelId=" + params.levelId;
        }
        if (params.loginIp !== '') {
            url += "&loginIp" + params.loginIp;
        }
        if (params.DateScope.length === 2) {
            url += "&DateScope=" + params.DateScope[0] + "&DateScope=" + params.DateScope[1];
        }
        //发送异步ajax请求
        await getRequest(url).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const newAccounts = resp.data;
                commit(CONCAT_ACCOUNTS, {newAccounts});
            }
        })
    },
    //异步改变指定账户的禁用状态
    async reverseAccountEnable({commit}, params) {
        // await putRequest("/api/userManage/enableReverse", params.data).then(resp => {
        await putRequest("/userManage/enableReverse", params.data).then(resp => {
            if (resp) {
                //这里的enable反转是用来更新Vuex中的禁用状态的,后端不看这个enable
                params.data.enable = !params.data.enable;
                /*//根据结果提交一个mutation
                commit(UPDATE_ACCOUNT_INFO, {params});*/
            }
        })
    },
    //异步重置指定账户的基本信息
    async resetAccount({commit}, params) {
        // await putRequest("/api/userManage/userReset", params.data).then(resp => {
        await putRequest("/userManage/userReset", params.data).then(resp => {
            if (resp) {
                params.data.loginIp = "0.0.0.0";
                params.data.loginTime = "1970-01-01 00:00:00";
                params.data.userProfile = "";
                params.data.userDescribe = "";
                params.data.enable = true;
                //根据结果提交一个mutation
                commit(UPDATE_ACCOUNT_INFO, {params});
            }
        })
    },

    /*------------------------------------区域管理------------------------------------*/
    //调整账户页面参数
    updateAreasParam({commit}, params) {
        commit(UPDATE_AREA_PARAMS, {params});
    },
    //异步获取区域信息
    async initAreas({commit}) {
        //判断是否有Area数据,如果有则略过避免重复请求
        if(!state.active.areaData[0]){
            //发送异步ajax请求
            // await getRequest("/api/area/getAreaList").then(resp => {
            await getRequest("/area/getAreaList").then(resp => {
                if (resp) {
                    //根据结果提交一个mutation
                    const areas = resp.obj;
                    commit(RECEIVE_AREAS, {areas});
                }
            })
        }
    },
    //异步新增单条区域信息
    async addArea({commit}, areaParam) {
        //发送异步ajax请求
        // await postRequest('/api/area/addArea', areaParam).then(resp => {
        await postRequest('/area/addArea', areaParam).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const newArea = resp.obj;
                commit(CONCAT_AREA, {newArea});
            }
        })
    },
    //异步修改指定区域的信息
    async editArea({commit}, params) {
        //发送异步ajax请求
        // await postRequest("/api/area/editArea", params.data).then(resp => {
        await postRequest("/area/editArea", params.data).then(resp => {
            if (resp) {
                params.data = JSON.parse(JSON.stringify(resp.obj));
                //提交一个mutation
                commit(UPDATE_AREA_INFO, {params});
            }
        })
    },
    //异步删除指定区域信息
    async deleteArea({commit}, params) {
        // await deleteRequest("/api/area/delete?areaID=" + params.data.areaId).then(resp => {
        await deleteRequest("/area/delete?areaID=" + params.data.areaId).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                commit(DELETE_AREA_INFO, {params});
            }
        })
    },

    /*------------------------------------设备管理------------------------------------*/
    //更新设备列表的数据查询参数
    updateEquipmentsParam({commit}, params) {
        commit(UPDATE_EQUIPMENT_PARAMS, {params})
    },
    //异步获取所有设备信息
    async getAllEquipments({commit}) {
        let total = state.active.equipmentData.total + '';
        let params = state.static.equipmentListParams;
        let size = params.pageSize;
        //异步提交查询
        // await getRequest("/api/equipmentManage/getEquipmentListWithPage?currentPage=1&size=" + total).then(resp => {
        await getRequest("/equipmentManage/getEquipmentListWithPage?currentPage=1&size=" + total).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const equipments = resp;
                commit(RECEIVE_EQUIPMENTS, {equipments});
                //按total和size的商向上取整设置currentPage,并更新查询参数
                params.currentPage = Math.ceil(total / size);
                commit(UPDATE_EQUIPMENT_PARAMS, {params});
            }
        })
    },
    //异步接受设备列表
    async initEquipments({commit}) {
        //判断是否有Equipment数据,如果有则略过避免重复请求
        if(!state.active.equipmentData.data){
            //发送异步ajax请求
            // await getRequest("/api/equipmentManage/getEquipmentListWithPage?currentPage=1&size=20").then(resp => {
            await getRequest("/equipmentManage/getEquipmentListWithPage?currentPage=1&size=20").then(resp => {
                if (resp) {
                    //根据结果提交一个mutation
                    const equipments = resp;
                    commit(RECEIVE_EQUIPMENTS, {equipments});
                }
            })
        }
    },
    //异步搜索设备信息
    async selectEquipments({commit}) {
        const params = state.static.equipmentListParams;
        //动态生成url
        // let url = "/api/equipmentManage/getEquipmentListWithPage?currentPage=1&size=20";
        let url = "/equipmentManage/getEquipmentListWithPage?currentPage=1&size=20";
        if (params.equipmentId !== '') {
            url += "&equipmentId=" + params.equipmentId;
        }
        if (params.areaId !== '') {
            url += "&areaId=" + params.areaId;
        }
        if (params.radius !== 0) {
            url += "&radius=" + params.radius;
        }
        if (params.state !== '') {
            url += "&state=" + params.state;
        }
        if (params.runDateScope.length === 2) {
            url += "&runDateScope=" + params.runDateScope[0] + "&runDateScope=" + params.runDateScope[1];
        }
        if (params.updateDataScope.length === 2) {
            url += "&updateDataScope=" + params.updateDataScope[0] + "&updateDataScope=" + params.updateDataScope[1];
        }
        //发送异步ajax请求
        await getRequest(url).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const equipments = resp;
                commit(RECEIVE_EQUIPMENTS, {equipments});
            }
        })
    },
    //异步新增单条设备信息
    async addEquipment({commit}, equipmentParam) {
        //发送异步ajax请求
        // await postRequest("/api/equipmentManage/addEquipment", equipmentParam).then(resp => {
        await postRequest("/equipmentManage/addEquipment", equipmentParam).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const newEquipment = resp.obj;
                commit(CONCAT_EQUIPMENT, {newEquipment});
            }
        })
    },
    //异步新增的多条数据到设备列表
    async addEquipments({commit}) {
        const params = state.static.equipmentListParams;
        //动态生成url
        // let url = "/api/equipmentManage/getEquipmentListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        let url = "/equipmentManage/getEquipmentListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        if (params.equipmentId !== '') {
            url += "&equipmentId=" + params.equipmentId;
        }
        if (params.areaId !== '') {
            url += "&areaId=" + params.areaId;
        }
        if (params.radius !== 0) {
            url += "&radius=" + params.radius;
        }
        if (params.state !== '') {
            url += "&state=" + params.state;
        }
        if (params.runDateScope.length === 2) {
            url += "&runDateScope=" + params.runDateScope[0] + "&runDateScope=" + params.runDateScope[1];
        }
        if (params.updateDataScope.length === 2) {
            url += "&updateDataScope=" + params.updateDataScope[0] + "&updateDataScope=" + params.updateDataScope[1];
        }
        //发送异步ajax请求
        await getRequest(url).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const newEquipments = resp.data;
                commit(CONCAT_EQUIPMENTS, {newEquipments});
            }
        })
    },
    //异步修改指定设备的信息
    async editEquipment({commit}, equipmentParam) {
        return new Promise(function (resolve,reject){
            //先判断此次更改有没有涉及到区域更改
            let equipmentData = state.active.equipmentData;
            let from = equipmentData.data.find(item => item.equipmentId === equipmentParam.data.equipmentId);
            if (from.areaId === equipmentParam.data.areaId) {
                //没有涉及到区域的变更
                // putRequest("/api/equipmentManage/editWithoutArea", equipmentParam.data).then(resp=>{
                putRequest("/equipmentManage/editWithoutArea", equipmentParam.data).then(resp=>{
                    if(resp){
                        //提交mutation
                        commit(UPDATE_EQUIPMENT_INFO, {equipmentParam});
                        resolve(true);
                    }else{
                        reject(false);
                    }
                })
            } else {
                //涉及到了区域变更
                // putRequest("/api/equipmentManage/editWithArea?from=" + from.areaId + "", equipmentParam.data).then(resp=>{
                putRequest("/equipmentManage/editWithArea?from=" + from.areaId + "", equipmentParam.data).then(resp=>{
                    if (resp) {
                        //提交两个mutation,一个是Equipment的数据修改,另一个是两个Area的数据修改
                        commit(UPDATE_EQUIPMENT_INFO, {equipmentParam});
                        let param = {
                            fromAreaId: from.areaId,
                            toAreaId: equipmentParam.data.areaId
                        }
                        commit(UPDATE_AREA_COUNT, {param});
                        resolve(true);
                    }else{
                        reject(false);
                    }
                })
            }
        })
    },
    async getEquipmentsByAreaID({commit},areaID){
        return new Promise(function (resolve,reject){
            //生成url
            // let url = "/api/equipmentManage/getEquipmentListWithPage?currentPage=1&size=100&areaId=" + areaID;
            let url = "/equipmentManage/getEquipmentListWithPage?currentPage=1&size=100&areaId=" + areaID;
            //发送请求
            getRequest(url).then(resp=>{
                if(resp){
                    resolve(resp.data);
                }else{
                    reject(false);
                }
            })
        })
    },

    /*------------------------------------辐射数据管理------------------------------------*/
    //更新辐射数据列表的查询参数
    updateDataParam({commit}, params) {
        commit(UPDATE_DATA_PARAMS, {params});
    },
    //异步接受辐射数据列表
    async initRadiationData({commit}) {
        //判断是否有RadiationData数据,如果有则略过避免重复请求
        if(!state.active.radiationData.data){
            //发送异步ajax请求
            // await getRequest("/api/radiationData/getDataListWithPage?currentPage=1&size=20").then(resp => {
            await getRequest("/radiationData/getDataListWithPage?currentPage=1&size=20").then(resp => {
                if (resp) {
                    //根据结果提交一个mutation
                    const radiationData = resp;
                    commit(RECEIVE_DATA, {radiationData});
                }
            })
        }
    },
    //异步新增多条辐射数据到辐射数据列表
    async addRadiationData({commit}) {
        const params = state.static.radiationListParams;
        //动态生成url
        // let url = "/api/radiationData/getDataListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        let url = "/radiationData/getDataListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        if (params.dataId !== '') {
            url += "&dataId=" + params.dataId;
        }
        if (params.alpha !== '') {
            url += "&alpha=" + params.alpha;
        }
        if (params.beta !== '') {
            url += "&beta=" + params.beta;
        }
        if (params.gamma !== '') {
            url += "&gamma=" + params.gamma;
        }
        if (params.humidity !== '') {
            url += "&humidity=" + params.humidity;
        }
        if (params.temperature !== '') {
            url += "&temperature=" + params.temperature;
        }
        if (params.windForce !== '') {
            url += "&windForce=" + params.windForce;
        }
        if (params.windDirection !== '') {
            url += "&windDirection=" + params.windDirection;
        }
        if (params.date.length === 2) {
            url += "&date=" + params.date[0] + "&date=" + params.date[1];
        }
        if (params.areaId !== '') {
            url += "&areaId=" + params.areaId;
        }
        if (params.equipmentId !== '') {
            url += "&equipmentId=" + params.equipmentId;
        }
        //必定带上控制搜索模式的参数
        url += "&isAlert=" + params.isAlert;
        //发送异步ajax请求
        await getRequest(url).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const newRadiationData = resp.data;
                commit(CONCAT_DATA, {newRadiationData});
            }
        })
    },
    //异步改变指定数据的禁用状态
    async reverseDataEnable({commit}, params) {
        // await putRequest("/api/radiationData/enableReverse", params.data).then(resp => {
        await putRequest("/radiationData/enableReverse", params.data).then(resp => {
            if (resp) {
                //这里的enable反转是用来更新Vuex中的禁用状态的,后端不看这个enable
                params.data.enable = !params.data.enable;
            }
        })
    },
    //异步搜索辐射数据信息
    async selectRadiationData({commit}) {
        const params = state.static.radiationListParams;
        //动态生成url
        // let url = "/api/radiationData/getDataListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        let url = "/radiationData/getDataListWithPage?currentPage=" + params.currentPage + "&size=" + params.pageSize;
        if (params.dataId !== '') {
            url += "&dataId=" + params.dataId;
        }
        if (params.alpha !== '') {
            url += "&alpha=" + params.alpha;
        }
        if (params.beta !== '') {
            url += "&beta=" + params.beta;
        }
        if (params.gamma !== '') {
            url += "&gamma=" + params.gamma;
        }
        if (params.humidity !== '') {
            url += "&humidity=" + params.humidity;
        }
        if (params.temperature !== '') {
            url += "&temperature=" + params.temperature;
        }
        if (params.windForce !== '') {
            url += "&windForce=" + params.windForce;
        }
        if (params.windDirection !== '') {
            url += "&windDirection=" + params.windDirection;
        }
        if (params.date.length === 2) {
            url += "&date=" + params.date[0] + "&date=" + params.date[1];
        }
        if (params.areaId !== '') {
            url += "&areaId=" + params.areaId;
        }
        if (params.equipmentId !== '') {
            url += "&equipmentId=" + params.equipmentId;
        }
        //必定带上控制搜索模式的参数
        url += "&isAlert=" + params.isAlert;
        //发送异步ajax请求
        await getRequest(url).then(resp => {
            if (resp) {
                //根据结果提交一个mutation
                const radiationData = resp;
                commit(RECEIVE_DATA, {radiationData});
            }
        })
    },
    //接受平台的基础统计数据
    async getBaseStatisticsData({commit}){
        return new Promise(function (resolve,reject){
            //发送请求
            // getRequest("/api/statisticsData/getBasicStatistics").then(resp=>{
            getRequest("/statisticsData/getBasicStatistics").then(resp=>{
                if(resp){
                    //接受数据
                    const statisticsData = resp.obj;
                    //替换area部分数组中对象的属性名
                    statisticsData.area.deployCountList = statisticsData.area.deployCountList.map(iterator=>{
                        return{
                            value: iterator.count,
                            name: iterator.areaName
                        }
                    })
                    //通过mutation提交到store中
                    commit(RECEIVE_BASE_STATISTICS_DATA,{statisticsData});
                    //返回成功信号
                    resolve(true);
                }else{
                    reject(false);
                }
            })
        })
    },
    //接受指定时间段的辐射统计数据
    async getStatisticsData({commit}, timeSpanIndex){
        return new Promise(function (resolve,reject){
            //发送请求
            // getRequest("/api/statisticsData/getDataStatistics?timeSpanIndex=" + timeSpanIndex).then(resp=>{
            getRequest("/statisticsData/getDataStatistics?timeSpanIndex=" + timeSpanIndex).then(resp=>{
                if(resp){
                    //接受数据
                    const originStatisticsData = resp.obj;
                    //创建基础格式架构
                    const statisticsData = {
                        xAxis: [],
                        areaName: [],
                        data: {
                            alpha: {
                                unit: "uSv/h",
                                data: []
                            },
                            beta: {
                                unit: "uSv/h",
                                data: []
                            },
                            gamma: {
                                unit: "uSv/h",
                                data: []
                            },
                            humidity: {
                                unit: "%",
                                data: []
                            },
                            temperature: {
                                unit: "℃",
                                data: []
                            },
                            windForce: {
                                unit: "级",
                                data: []
                            }
                        }
                    };
                    //用于临时保存单区域数据的数组
                    let alphaArray = [];
                    let betaArray = [];
                    let gammaArray = [];
                    let humidityArray = [];
                    let temperatureArray = [];
                    let windForceArray = [];
                    //用于处理xAxis数据注入时,仅首次遍历时注入数据用
                    let isFirst = true;

                    //处理数据结构
                    originStatisticsData.map(item=>{
                        //每次遍历时清空临时保存数据用的数组
                        alphaArray = [];
                        betaArray = [];
                        gammaArray = [];
                        humidityArray = [];
                        temperatureArray = [];
                        windForceArray = [];
                        //每次遍历把区域名称存入数组
                        statisticsData.areaName.push(item.areaName);
                        //每次遍历存入各类型数据到对应数组
                        item.statisticsAreaData.map(member=>{
                            if(isFirst){
                                statisticsData.xAxis.push(member.date);
                            }
                            //存入各项数据到对应临时数组中
                            alphaArray.push(member.AVG_alpha>=0?member.AVG_alpha:"无数据");
                            betaArray.push(member.AVG_beta>=0?member.AVG_beta:"无数据");
                            gammaArray.push(member.AVG_gamma>=0?member.AVG_gamma:"无数据");
                            humidityArray.push(member.AVG_humidity>=0?member.AVG_humidity:"无数据");
                            temperatureArray.push(member.AVG_temperature>=0?member.AVG_temperature:"无数据");
                            windForceArray.push(member.AVG_windForce>=0?member.AVG_windForce:"无数据");
                        })
                        //将临时数组放入结构体中(深拷贝)
                        statisticsData.data.alpha.data.push(JSON.parse(JSON.stringify(alphaArray)));
                        statisticsData.data.beta.data.push(JSON.parse(JSON.stringify(betaArray)));
                        statisticsData.data.gamma.data.push(JSON.parse(JSON.stringify(gammaArray)));
                        statisticsData.data.humidity.data.push(JSON.parse(JSON.stringify(humidityArray)));
                        statisticsData.data.temperature.data.push(JSON.parse(JSON.stringify(temperatureArray)));
                        statisticsData.data.windForce.data.push(JSON.parse(JSON.stringify(windForceArray)));
                        //修改"首次"标记为false,避免xAxis重复注入
                        isFirst = false;
                    })

                    //通过mutation提交到store中
                    commit(RECEIVE_STATISTICS_DATA,{statisticsData});
                    //返回成功信号
                    resolve(true);
                }else{
                    reject(false);
                }
            })
        })
    },

    /*------------------------------------记录管理------------------------------------*/
    //接受近期操作记录列表
    async initRecords({commit}){
        //判断是否有Record数据,如果有则略过避免重复请求
        if(!state.active.recordData.account){
            //发送异步ajax请求
            // await getRequest("/api/record/getRecord").then(resp => {
            await getRequest("/record/getRecord").then(resp => {
                if(resp){
                    //根据结果提交mutation
                    const records = resp.obj;
                    commit(RECEIVE_RECORDS, {records});
                }
            })
        }
    }
}