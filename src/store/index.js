import Vue from 'vue'
import Vuex from 'vuex'
import state from "./state";
import action from "./actions";
import mutation from "./mutations";
import getter from "./getters";

Vue.use(Vuex)

export default new Vuex.Store({
    //用来保存所有组件的公共数据
    state: state,
    //存放用于改变state中存储的值的方法(同步执行的方法)
    mutations: mutation,
    //存放用于改变state中存储的值的方法(异步执行的方法)
    actions: action,
    modules: {}
})
