// 包含n个mutation的type名称常量

//个人设置
export const UPDATE_LOGIN_INFO = 'update_login_info'                //更新登录者的数据信息
export const UPDATE_LOGIN_PROFILE = 'update_login_profile'          //更新登录者的头像url

//账户信息管理
export const UPDATE_ACCOUNTS_PARAMS = 'update_accounts_params'      //更新账户列表的数据查询参数
export const RECEIVE_ACCOUNTS = 'receive_accounts'                  //接受初始账户列表
export const CONCAT_ACCOUNT = 'concat_account'                      //补充新增的单条数据到账户列表
export const CONCAT_ACCOUNTS = 'concat_accounts'                    //补充新增的多条数据到账户列表
export const UPDATE_ACCOUNT_INFO = 'update_account_info'            //更新本地保存的指定账户信息

//区域信息管理
export const UPDATE_AREA_PARAMS = 'update_area_params'              //更新区域列表的数据查询参数
export const RECEIVE_AREAS = 'receive_areas'                        //接受初始区域列表
export const CONCAT_AREA = 'concat_area'                            //补充新增的单条数据到区域列表
export const UPDATE_AREA_INFO = 'update_area_info'                  //更新本地保存的指定区域信息
export const UPDATE_AREA_COUNT = 'update_area_count'                //更新本地的两个区域信息的count值
export const DELETE_AREA_INFO = 'delete_area_info'                  //删除本地保存的指定区域信息

//设备信息管理
export const UPDATE_EQUIPMENT_PARAMS = 'update_equipment_params'    //更新设备列表的数据查询参数
export const RECEIVE_EQUIPMENTS = 'receive_equipments'              //接受初始设备列表
export const CONCAT_EQUIPMENT = 'concat_equipment'                  //补充新增的单条数据到设备列表
export const CONCAT_EQUIPMENTS = 'concat_equipments'                //补充新增的多条数据到设备列表
export const UPDATE_EQUIPMENT_INFO = 'update_equipment_info'        //更新本地保存的指定设备信息

//辐射数据管理
export const UPDATE_DATA_PARAMS = 'update_data_params'              //更新辐射数据列表的查询参数
export const RECEIVE_DATA = 'receive_data'                          //接受初始辐射数据列表
export const CONCAT_DATA = 'concat_data'                            //补充新增的多条数据到辐射数据列表
export const UPDATE_DATA_INFO = 'update_data_info'                  //更新本地保存的指定辐射数据列表(没用上...)
export const RECEIVE_BASE_STATISTICS_DATA = 'receive_base_statistics_data'    //接受平台的基础统计数据
export const RECEIVE_STATISTICS_DATA = 'receive_statistics_data'    //接受指定时间段的辐射统计数据

//操作记录管理
export const RECEIVE_RECORDS = 'receive_records'                    //接受近期操作记录列表