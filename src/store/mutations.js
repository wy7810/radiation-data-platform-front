// 直接更新state的多个方法的对象

import {
    //个人设置
    UPDATE_LOGIN_INFO,
    UPDATE_LOGIN_PROFILE,
    //账户管理
    UPDATE_ACCOUNTS_PARAMS,
    RECEIVE_ACCOUNTS,
    CONCAT_ACCOUNT,
    CONCAT_ACCOUNTS,
    UPDATE_ACCOUNT_INFO,
    //区域管理
    UPDATE_AREA_PARAMS,
    RECEIVE_AREAS,
    CONCAT_AREA,
    UPDATE_AREA_INFO,
    UPDATE_AREA_COUNT,
    DELETE_AREA_INFO,
    //设备管理
    UPDATE_EQUIPMENT_PARAMS,
    RECEIVE_EQUIPMENTS,
    CONCAT_EQUIPMENT,
    CONCAT_EQUIPMENTS,
    UPDATE_EQUIPMENT_INFO,
    //辐射数据管理
    UPDATE_DATA_PARAMS,
    RECEIVE_DATA,
    CONCAT_DATA,
    UPDATE_DATA_INFO,
    RECEIVE_BASE_STATISTICS_DATA,
    RECEIVE_STATISTICS_DATA,
    //操作记录管理
    RECEIVE_RECORDS
} from "./mutationTypes";

export default {
    /*------------------------------------个人设置------------------------------------*/
    [UPDATE_LOGIN_INFO](state, {params}) {
        state.static.loginInfo = params;
    },
    [UPDATE_LOGIN_PROFILE](state, {params}) {
        state.static.profileURL = params;
    },

    /*------------------------------------账户管理------------------------------------*/
    //更新账户列表的数据查询参数
    [UPDATE_ACCOUNTS_PARAMS](state, {params}) {
        //深拷贝
        state.static.accountListParams = JSON.parse(JSON.stringify(params));
    },
    //接受账户列表
    [RECEIVE_ACCOUNTS](state, {accounts}) {
        state.active.accountData = accounts;
    },
    //补充新增的单条数据到账户列表
    [CONCAT_ACCOUNT](state, {newAccount}) {
        state.active.accountData.data = state.active.accountData.data.concat(newAccount);
        state.active.accountData.total++;
    },
    //补充新增的多条数据到账户列表
    [CONCAT_ACCOUNTS](state, {newAccounts}) {
        state.active.accountData.data = state.active.accountData.data.concat(newAccounts);
    },
    //更新本地保存的指定账户信息(好像没用上...)
    [UPDATE_ACCOUNT_INFO](state, {params}) {
        let index = state.active.accountData.data.findIndex(item=>item.userId === params.data.userId);
        state.active.accountData.data.splice(index, 1, params.data);
    },

    /*------------------------------------区域管理------------------------------------*/
    //更新区域列表的数据查询参数
    [UPDATE_AREA_PARAMS](state, {params}) {
        state.static.areaListParams = params;
    },
    //接受区域列表
    [RECEIVE_AREAS](state, {areas}) {
        state.active.areaData = areas;
    },
    //补充新增的单条数据到区域列表
    [CONCAT_AREA](state, {newArea}) {
        state.active.areaData = state.active.areaData.concat(newArea);
    },
    //更新本地保存的指定区域信息
    [UPDATE_AREA_INFO](state, {params}) {
        //通过ID在areaData中查找要修改的对象的index,再通过splice修改
        let index = state.active.areaData.findIndex(item => item.areaId === params.data.areaId)
        let areaArray = state.active.areaData;
        if (index !== -1) {
            areaArray.splice(index, 1, params.data);
            //同时还需要修改equipmentData中所有归属于该区域的设备数据中的area对象内容
            let equipmentArray = state.active.equipmentData.data;
            //如果本地State有关于equipment的数据才会进行修改
            if (equipmentArray) {
                for (let item of equipmentArray) {
                    if (item.areaId === params.data.areaId) {
                        //深拷贝
                        item.area = JSON.parse(JSON.stringify(params.data));
                    }
                }
            }
        }
    },
    //更新本地的两个区域信息的count值
    [UPDATE_AREA_COUNT](state, {param}) {
        let areaArray = state.active.areaData;
        //params中含有toAreaId、fromAreaId
        let fromIndex = state.active.areaData.findIndex(item => item.areaId === param.fromAreaId);
        let toIndex = state.active.areaData.findIndex(item => item.areaId === param.toAreaId);
        if ((fromIndex !== -1) && (toIndex !== -1)) {
            areaArray[fromIndex].count--;
            areaArray[toIndex].count++;
            //同时还需要修改equipmentData中所有归属于该区域的设备数据中的area对象内容
            let equipmentArray = state.active.equipmentData.data;
            //如果本地State有关于equipment的数据才会进行修改
            if (equipmentArray) {
                for (let item of equipmentArray) {
                    if (item.areaId === areaArray[fromIndex].areaId) {
                        //深拷贝
                        item.area = JSON.parse(JSON.stringify(areaArray[fromIndex]));
                    } else if (item.areaId === areaArray[toIndex].areaId) {
                        //深拷贝
                        item.area = JSON.parse(JSON.stringify(areaArray[toIndex]));
                    }
                }
            }
        }
    },
    //删除本地保存的指定区域信息
    [DELETE_AREA_INFO](state, {params}) {
        let dataArray = state.active.areaData;
        let index = dataArray.findIndex(item=>item.areaId === params.data.areaId);
        dataArray.splice(index, 1);
    },

    /*------------------------------------设备管理------------------------------------*/
    //更新设备列表的数据查询参数
    [UPDATE_EQUIPMENT_PARAMS](state, {params}) {
        //深拷贝
        state.static.equipmentListParams = JSON.parse(JSON.stringify(params));
    },
    //接受设备列表
    [RECEIVE_EQUIPMENTS](state, {equipments}) {
        if (state.active.equipmentData.total && (state.active.equipmentData.total > equipments.total)) {
            state.active.equipmentData.data = equipments.data
        } else {
            state.active.equipmentData = equipments;
        }
    },
    //补充管理者手动添加的单条数据到设备列表
    [CONCAT_EQUIPMENT](state, {newEquipment}) {
        state.active.equipmentData.data = state.active.equipmentData.data.concat(newEquipment);
        state.active.equipmentData.total++;
        //如果总页面超出当前页面限制,则登记的当前页面+1
        if ((state.active.equipmentData.total) % (state.static.equipmentListParams.pageSize) === 1) {
            state.static.equipmentListParams.currentPage++;
        }
        //同时在对应的Area中增加count值
        let index = state.active.areaData.findIndex(item => item.areaId === newEquipment.areaId);
        if (index !== -1) {
            state.active.areaData[index].count++;
        }
    },
    //补充来自服务器请求响应的多条数据到设备列表
    [CONCAT_EQUIPMENTS](state, {newEquipments}) {
        state.active.equipmentData.data = state.active.equipmentData.data.concat(newEquipments);
    },
    //修改指定设备的信息
    [UPDATE_EQUIPMENT_INFO](state, {equipmentParam}) {
        let dataArray = state.active.equipmentData.data;
        //清除改组数据的securityCode
        equipmentParam.data.securityCode = '';
        let index = dataArray.findIndex(item=>item.equipmentId === equipmentParam.data.equipmentId);
        dataArray.splice(index, 1, equipmentParam.data);
    },

    /*------------------------------------辐射数据管理------------------------------------*/
    //更新辐射数据列表的查询参数
    [UPDATE_DATA_PARAMS](state,{params}){
        //深拷贝
        state.static.radiationListParams = JSON.parse(JSON.stringify(params));
    },
    //接受辐射数据列表
    [RECEIVE_DATA](state, {radiationData}) {
        /*if (state.active.radiationData.total && (state.active.radiationData.total > radiationData.total)) {
            state.active.radiationData.data = radiationData.data
        } else {
            state.active.radiationData = radiationData;
        }*/

        state.active.radiationData = radiationData;

    },
    //补充来自服务器请求响应的多条数据到辐射数据表
    [CONCAT_DATA](state,{newRadiationData}){
        state.active.radiationData.data = state.active.radiationData.data.concat(newRadiationData)
    },
    //更新本地的辐射数据信息(没用上...)
    [UPDATE_DATA_INFO](state,{params}){
        let index = state.active.radiationData.data.findIndex(item=>item.dataId === params.data.dataId);
        state.active.radiationData.data.splice(index,1,params.data);
    },
    //接受平台的基础统计数据
    [RECEIVE_BASE_STATISTICS_DATA](state,{statisticsData}){
        //深拷贝
        state.active.baseStatisticsData = JSON.parse(JSON.stringify(statisticsData));
    },
    //接受指定时间段的辐射统计数据
    [RECEIVE_STATISTICS_DATA](state,{statisticsData}){
        //深拷贝
        // state.active.statisticsData = JSON.parse(JSON.stringify(statisticsData));
        state.active.statisticsData = statisticsData;
    },

    /*------------------------------------记录管理------------------------------------*/
    [RECEIVE_RECORDS](state,{records}){
        state.active.recordData = records;
    }
}