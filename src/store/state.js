import citys from '../assets/citys.json';
import wind from '../assets/wind.json'

// 状态对象
export default {
    //动态数据,不存入sessionStorage
    active: {
        radiationData: {},
        accountData: {},
        areaData: [],
        equipmentData: {},
        baseStatisticsData: {},
        statisticsData: {},
        recordData: {}
    },
    //静态数据,存入sessionStorage中以防刷新页面后丢失
    static: sessionStorage.getItem("static")?JSON.parse(sessionStorage.getItem("static")):{
        citys,
        wind,
        loginInfo: {},
        profileURL: '',
        accountListParams: {
            currentPage: 1,
            pageSize: 20,
            userId: '',
            userName: '',
            loginIp: '',
            levelId: '',
            DateScope: []
        },
        radiationListParams: {
            currentPage: 1,
            pageSize: 20,
            dataId: '',
            alpha: '',
            beta: '',
            gamma: '',
            humidity: '',
            temperature: '',
            windForce: '',
            windDirection: '',
            date: [],
            areaId: '',
            equipmentId: '',
            isAlert: 0
        },
        equipmentListParams: {
            currentPage: 1,
            pageSize: 20,
            equipmentId: '',
            areaId: '',
            state: '',
            radius: 0,
            runDateScope: [],
            updateDataScope: []
        }
    }

}