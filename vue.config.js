
let proxyObj={};

proxyObj['/']={
    //websocket的开关设置
    ws:false,
    //需要代理到的目标地址
    target:'http://localhost:8081',
    //发送请求头中的host是否会被设置成target
    changeOrigin: true,
    //地址重写
    pathRewrite:{
        //不重写请求地址
        '^/':'/'
    }
}

module.exports = {
    publicPath: "./",
    //devServer,如同其名称代表的,只用于本地开发机测试时的代理配置,正式发布后的项目请求代理遵循Nginx或Tomcat的代理配置文件
    devServer:{
        host:'localhost',
        port:8080,
        proxy:proxyObj
    }
}