# radiation-data-platform-front

## 介绍

一款接收指定设备采集到的辐射数据并储存，以供用户查询的数据平台的前端项目文件

## 软件架构

本数据平台采用前后端分离的方式进行开发：  
前端：JavaScript + Vue.js + Axios + ElementUI + ECharts + CSS3/HTML5  
后端：JavaSE + Spring Boot + JWT验证 + Mybatis  
数据库：MySQL + Redis

## 使用说明
通过命令构建项目包，并放置在前端服务器中运行。前端项目为用户提供访问页面，并负责与后端项目进行数据上的异步交互。

### 启用项目
```
npm run serve
```

### 构建项目
```
npm run build
```
